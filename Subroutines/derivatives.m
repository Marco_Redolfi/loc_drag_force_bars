% ************************************************************************************
% CALCULATIONS OF COEFFICIENTS THAT DEFINE THE SENSITIVITY OF CHÉZY COEFFICIENT AND
% SEDIMENT FLUX TO VARIATIONS OF WATER DEPTH AND SHIELDS STRESS
% Author: Marco Redolfi (marco.redolfi@unitn.it)
% Created on: 25-Feb-2019
% ************************************************************************************

function [c0,cD,cT,PhiD,PhiT]=derivatives(theta0,ds0,resistance_f,transport_f,c0,cD,cT)

% INPUTS
% theta0:       Shields stress [-]
% ds0:          relative grain size [-]
% resistance_f: resistance formula [string]
%      current options: 'Engelund&Hansen'; 'Engelund&Hansen_dunes'; 'Strickler'; 'Wilkerson&Parker'
% transport_f:  transport formula [string]
%      current options: 'MP&M'; 'Wong&Parker'; 'Parker'; 'Engelund&Hansen'; 'Van_Rijn'
% c0:           user-provided (i.e. independent on ds0) value of the reference Chézy coefficient
% cD:           user-provided value
% cT:           user-provided value


%% Definition of c0,cD,cT coefficients depending on the resistance formula

switch lower(resistance_f)
    
    case lower('Engelund&Hansen') %Engelund and Hansen (1967) formula (flat bed)

        if isempty(c0)
            c0=6.+2.5*log(1./(2.5*ds0));
        end

        if isempty(cD); cD=2.5/c0; end
        if isempty(cT); cT=0;      end

    case lower('Engelund&Hansen_dunes') %Engelund and Hansen (1967) formula (dunes)

        f0=(0.06+0.4*theta0^2)/theta0; 
        
        if isempty(c0)
            c0=sqrt(f0)*(6.+2.5*log(f0/(2.5*ds0)));
        end

        if isempty(cD); cD=2.5*sqrt(f0)/c0; end
        if isempty(cT)
            fT=theta0/f0*(-0.06/(theta0^2)+0.4); %Relative variation of f with theta
            cT=(1/2+2.5*sqrt(f0)/c0)*fT; 
        end

    case lower('Strickler') %Strickler (1923) formula

        if isempty(c0)
            c0=21.1/(9.81^0.5)/ds0^(1/6);
        end
        if isempty(cD); cD=1/6; end
        if isempty(cT); cT=0;   end

    case lower('Wilkerson&Parker') %Wilkerson and Parker (2011) formula

        if isempty(c0)
            c0=4.88/ds0^0.083;
        end
        if isempty(cD); cD=0.083; end
        if isempty(cT); cT=0;     end

    otherwise

        error('Error: Unknown resistance formula!')

end


%% Definition of PhiD and PhiT coefficients depending on the transport formula

switch lower(transport_f) 

    case lower('MP&M') %Meyer-Peter and Muller (1948) formula
        theta_cr=0.047; %Critical value of theta 

        if theta0<=theta_cr
            warning('theta<=theta_cr')
    
            %Return NaN values
            beta_R=NaN;
            wavenumber_R=NaN;
            return
        end
        
        %Phi0=8.*(theta0-theta_cr)^1.5;
        PhiT=1.5*theta0/(theta0-theta_cr);
        PhiD=0;

    case lower('Wong&Parker') %Wong and Parker (1996)

        theta_cr=0.047; %Critical value of theta 

        if theta0<=theta_cr
            warning('theta<=theta_cr')
            
            %Return NaN values
            beta_R=NaN;
            wavenumber_R=NaN;
            return
        end
        %Phi0=4.93.*(theta0-theta_cr)^1.6;
        PhiT=1.6*theta0/(theta0-theta_cr);
        PhiD=0;

    case lower('Parker') %Parker (1990)

        %Parameters
        A=0.0386;
        B=0.853;
        C=5474;
        D=0.00218;

        x=theta0/A;	%Normalized Shields stress in Parker formula
        if x>1.59
            Phi0=C*D*theta0^1.5*(1-B/x)^4.5;
            Phi_der=1.5*theta0^0.5*(1-B/x)^4.5*C*D+4.5*A*B*(1-B/x)^3.5*C*D/(theta0^0.5);
        elseif x>1
            Phi0=D*(theta0^1.5)*(exp(14.2*(x-1)-9.28*(x-1)^2));
            Phi_der=1/A*Phi0*(14.2-9.28*2*(x-1)) + 1.5*Phi0/theta0;
        else
            Phi0=D*theta0^1.5*x^14.2;
            Phi_der=14.2/A*D*theta0^1.5*x^13.2 + D*x^14.2*1.5*theta0^0.5;
        end

        PhiT=theta0/Phi0*Phi_der;
        PhiD=0;

    case lower('Engelund&Hansen') %Engelund and Hansen (1967)

        %alpha_EH=1;
        %Phi0=0.05*alpha_EH*c0^2*(theta0)^2.5;
        PhiT=2.5;
        PhiD=2*cD;    

    case lower('Van_Rijn') %Van Rijn (1984)  

        theta_cr=0.055;

        if theta0<=theta_cr
            warning('theta<=theta_cr')
    
            %Return NaN values
            beta_R=NaN;
            wavenumber_R=NaN;
            return
        end

        if theta0/(theta0-theta_cr)<3.0
            PhiT=2.1*theta0/(theta0-theta_cr);
        else
            PhiT=1.5*theta0/(theta0-theta_cr);
        end    
        PhiD=0;

    otherwise

        error('Unknown transport formula!')

end


return
