% PLOTTING DIMENSIONAL COLOR MAPS AND LONGITUDINAL PROFILES
% Modified on: 19-Aug-2019

disp('Printing dimensional plots..')


Dstar=Bstar/beta0; %Reference water depth [m]


%% 1) Longitudinal profiles at the two banks

%Bed elevation
figure

	plL=plot([x_A,x_B]*Bstar,[eta_A(end,:),eta_B(end,:)]*Dstar,'-r');
	hold on
     
      	plR=plot([x_A,x_B]*Bstar,[eta_A(1,:),eta_B(1,:)]*Dstar,'-b');
  
        y_lim=get(gca,'Ylim');
        plG=plot([0 0],y_lim,'-g','LineWidth',2);
       	uistack(plR,'top')
        set(gca,'Ylim',y_lim)

	hold off	

	grid on
	xlabel('x^* [m]')
	ylabel('Bed elevation \eta^* [m]')
	legend([plL,plR,plG],'Left bank','Right bank','Grid','Location','NE')

	printpdf(['Figures/Bed_elevation_profiles_dimensional.pdf'],[0 0.1 0 0],gcf,[15 5])


%Water surface elevation
figure
	
    plL=plot([x_A,x_B]*Bstar,[eta_A(end,:)+D_A(end,:),eta_B(end,:)+D_B(end,:)]*Dstar,'-r');
	hold on

      	plR=plot([x_A,x_B]*Bstar,[eta_A(1,:)+D_A(1,:),eta_B(1,:)+D_B(1,:)]*Dstar,'-b');

        y_lim=get(gca,'Ylim');
        plot([0 0],y_lim,'-g','LineWidth',2);
       	uistack(plR,'top')
        set(gca,'Ylim',y_lim)
	hold off	
	
	grid on
	xlabel('x^* [m]')
	ylabel('Water surface elevation [m]')
	legend([plL,plR,plG],'Left bank','Right bank','Grid','Location','NE')

	printpdf(['Figures/WSE_profiles_dimensional.pdf'],[0 0.1 0 0],gcf,[15 5])
	

	
%% 2) 2-D maps

figure

colormap jet

subplot(4,1,1)
   
	pcolor([XA XB]*Bstar,[YA YB]*Bstar,[eta_A eta_B]*Dstar)
    hold on
        plot([0 0],[2-delta,2]*Bstar,'-k','LineWidth',1.5)
    hold off    

    title('Bed elevation \eta^* [m]')
	axis equal
	ylim([0 2]*Bstar)
    shading interp
    xlabel('x^* [m]')
    ylabel('y^* [m]')
    colorbar

subplot(4,1,2)

	pcolor([XA XB]*Bstar,[YA YB]*Bstar,[U_A U_B]*Ustar)
    hold on
        plot([0 0],[2-delta,2]*Bstar,'-k','LineWidth',1.5)
    hold off    

    title('Longitudinal velocity U^* [m/s]')
	axis equal
	ylim([0 2]*Bstar)
    shading interp
    xlabel('x^* [m]')
    ylabel('y^* [m]')
    colorbar


subplot(4,1,3)

	pcolor([XA XB]*Bstar,[YA YB]*Bstar,[V_A V_B]*Ustar)
    hold on
        plot([0 0],[2-delta,2]*Bstar,'-k','LineWidth',1.5)
    hold off    

    title('Transverse velocity V^* [m/s]')
	axis equal
	ylim([0 2]*Bstar)
    shading interp
    xlabel('x^* [m]')
    ylabel('y^* [m]')
    colorbar


subplot(4,1,4)

	pcolor([XA XB]*Bstar,[YA YB]*Bstar,[eta_A+D_A eta_B+D_B]*Dstar)
    hold on
        plot([0 0],[2-delta,2]*Bstar,'-k','LineWidth',1.5)
    hold off    
    
    title('Water surface elevation \eta^*+D^* [m]')
	axis equal
	ylim([0 2]*Bstar)
    shading interp
    xlabel('x^* [m]')
    ylabel('y^* [m]')
    colorbar
	
	printpdf(['Figures/Maps_dimensional.pdf'],[-1 -0.5 1 1],gcf,[22 14],'-r600','zbuffer')
