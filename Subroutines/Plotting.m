% PLOTTING DIMENSIONLESS COLOR MAPS AND LONGITUDINAL PROFILES
% Modified on: 08-Aug-2019

disp('Printing dimensionless plots..')

%% 1) Matching conditions between the two channels	
figure

	plot(y,eta_A(:,end),'-b');
	hold on
		plot(y,eta_B(:,1),'-r');
	hold off
		
	grid on
	xlabel('y [-]')
	ylabel('Bed elevation \eta [-]')
	legend('\eta_A','\eta_B','Location','NW')
	
    printpdf(['Figures/Matching.pdf'],[0 0.2 0 0],gcf,[15 10])
	
		
%% 2) Longitudinal profiles at the two banks

%Bed elevation
figure

	plL=plot([x_A,x_B],[eta_A(end,:),eta_B(end,:)],'-r');
	hold on

      	plR=plot([x_A,x_B],[eta_A(1,:),eta_B(1,:)],'-b');
  
        y_lim=get(gca,'Ylim');
        plG=plot([0 0],y_lim,'-g','LineWidth',2);
       	uistack(plR,'top')
        set(gca,'Ylim',y_lim)

	hold off	
	
	grid on
	xlim([-L_A,L_B])
	%ylim([-0.3 0.9])
	xlabel('x [-]')
	ylabel('Bed elevation \eta [-]')
	legend([plL,plR,plG],'y=2 (left bank)','y=0 (right bank)','Grid','Location','NE')

	printpdf(['Figures/Bed_elevation_profiles.pdf'],[0 0.1 0 0],gcf,[15 5])
	

%Longitudinal velocity
figure
	
	plL=plot([x_A,x_B],[U_A(end,:),U_B(end,:)],'-r');
	hold on

      	plR=plot([x_A,x_B],[U_A(1,:),U_B(1,:)],'-b');
  
        y_lim=get(gca,'Ylim');
        plG=plot([0 0],y_lim,'-g','LineWidth',2);
       	uistack(plR,'top')
        set(gca,'Ylim',y_lim)

	hold off	
	
	grid on
	xlim([-L_A,L_B])
	xlabel('x [-]')
	ylabel('Longitudinal velocity U [-]')
	legend([plL,plR,plG],'y=2 (left bank)','y=0 (right bank)','Grid','Location','NE')

	printpdf(['Figures/Longitudinal_velocity_profiles.pdf'],[0 0.1 0 0],gcf,[15 5])
	

%Water surface elevation
figure
	
    plL=plot([x_A,x_B],[eta_A(end,:)+D_A(end,:),eta_B(end,:)+D_B(end,:)],'-r');
	hold on

      	plR=plot([x_A,x_B],[eta_A(1,:)+D_A(1,:),eta_B(1,:)+D_B(1,:)],'-b');
  
        y_lim=get(gca,'Ylim');
        plG=plot([0 0],y_lim,'-g','LineWidth',2);
       	uistack(plR,'top')
        set(gca,'Ylim',y_lim)

	hold off	
	
	grid on
	xlim([-L_A,L_B])
	xlabel('x [-]')
	ylabel('Water surface elevation [-]')
	legend([plL,plR,plG],'y=2 (left bank)','y=0 (right bank)','Grid','Location','NE')

	printpdf(['Figures/WSE_profiles.pdf'],[0 0.1 0 0],gcf,[15 5])
	

	
%% 3) 2-D maps

figure

colormap jet

subplot(4,1,1)
   
	pcolor([XA XB],[YA YB],[eta_A eta_B])
    hold on
        plot([0 0],[2-delta,2],'-k','LineWidth',1.5)
    hold off    

    title('Bed elevation \eta [-]')
	axis equal
	ylim([0 2])
    shading interp
    xlabel('x [-]')
    ylabel('y [-]')
    colorbar

subplot(4,1,2)

	pcolor([XA XB],[YA YB],[U_A U_B])
    hold on
        plot([0 0],[2-delta,2],'-k','LineWidth',1.5)
    hold off    

    title('Longitudinal velocity U [-]')
	axis equal
	ylim([0 2])
    shading interp
    xlabel('x [-]')
    ylabel('y [-]')
    colorbar


subplot(4,1,3)

	pcolor([XA XB],[YA YB],[V_A V_B])
    hold on
        plot([0 0],[2-delta,2],'-k','LineWidth',1.5)
    hold off    

    title('Transverse velocity V [-]')
	axis equal
	ylim([0 2])
    shading interp
    xlabel('x [-]')
    ylabel('y [-]')
    colorbar


subplot(4,1,4)

	pcolor([XA XB],[YA YB],[eta_A+D_A eta_B+D_B])
    hold on
        plot([0 0],[2-delta,2],'-k','LineWidth',1.5)
    hold off    
    
    title('Water surface elevation \eta+D [-]')
	axis equal
	ylim([0 2])
    shading interp
    xlabel('x [-]')
    ylabel('y [-]')
    colorbar
	
	printpdf(['Figures/Maps.pdf'],[-1 -0.5 1 1],gcf,[22 14],'-r600','zbuffer')
