%****************************************************************************************
% SOLUTION FOR THE COEFFICIENTS ETA_TILDE THAT ENSURES THE MATCHING CONDITIONS AT x=0
% Author: Marco Redolfi (marco.redolfi@unitn.it)
% Modified on: 14-Aug-2019 
%****************************************************************************************

function S=solution(N,u,v,d,lambda,delta,Fr0)

for m=1:N
	%Linear system in the form A*S=B

	%Rows (4): matching conditions for eta,U,V,D
	%Colums (8): unknown coefficients
    %   1->eta_tilde_1_B
    %   2->eta_tilde_2_B (sub-res) or eta_tilde_2_A (super-res)
    %   3->eta_tilde_3_B (sub-res) or eta_tilde_3_A (super-res) 
    %   4->eta_tilde_4_A
	
	%Sign of the eigenvalues:
    % if positive -> the eigenvalue belongs to channel A
    % if negative -> the eigenvalue belongs to channel B 
    sgn=sign(real(lambda(m,:))); 
    
    %Expansion coefficients for the indication function I(y)
	Am=-2/(m*pi)*sin(m*pi*(1-delta/2));
	
	%Vector of known terms
	B(1)=Am*Fr0^2; %Forcing component due to the grid
	B(2)=0;
	B(3)=0;
	B(4)=0;

    %Buildig matrix of coefficients
	A=zeros(4,4);

    %POsitive sign if the eigenvalue belongs to channel A
	A(1,:)=1      *sgn; 
	A(2,:)=u(m,:).*sgn;
	A(3,:)=v(m,:).*sgn;
    A(4,:)=d(m,:).*sgn; 
    
	S(m,:)=linsolve(A,transpose(B)); %Solution of the linear system
end

return
