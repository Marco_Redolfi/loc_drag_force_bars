% ************************************************************************************
% FUNCTION FOR COMPUTING EIGENVALUES OF THE LINEAR, 2D, SHALLOW WATER-EXNER SYSTEM
% Author: Marco Redolfi (marco.redolfi@unitn.it)
% Created on: 25-Feb-2019
% Modified on: 12-Aug-2019
% ************************************************************************************

function [lambda_mj,u_mj,v_mj,d_mj,beta_R,wavenumber_R]=eigenvalues(beta0,theta0,ds0,N,varargin)
	
% OUTPUTS
% lambda_mj:    four wavenumbers for each mode [-]
% u_mj:         u-coefficients for each mode [-]
% v_mj:         v-coefficients for each mode [-]
% d_mj:         d-coefficients for each mode [-]
% beta_R:       resonant aspect ratio [-]
% wavenumber_R: resonant wavenumber [-]


% INPUTS
% beta0:  Half width-to-depth ratio [-]
% theta0: Shields stress [-]
% ds0:    relative grain size [-]
% M:      Number of Fourier modes [-]

% OPTIONAL PARAMETERS
% r:               Ikeda parameter for gravitational effect on bed load [-] (default=0.5)
% transport_f:     transport formula [string] (default='Parker')
%      current options: 'MP&M'; 'Wong&Parker'; 'Parker'; 'Engelund&Hansen'; 'Van_Rijn'
% resistance_f:    resistance formula [string] (default='Engelund&Hansen')
%      current options: 'Engelund&Hansen'; 'Engelund&Hansen_dunes'; 'Strickler'; 'Wilkerson&Parker'
% c0:              user-provided value of dimensionless Chèzy coefficient
% cD:              user-provided value of the coefficient cD
% cT:              user-provided value of the coefficient cT
% Fr0:             user-provided value of the Froude number
% Delta:           relative submerged density of sediment [-] (default=1.65)
% sec_flow:        false (default)->no spiral flow effect
%                  true-> spiral flow based on the curvature of the streamlines

% ************************************************************************************

%Parsing optional inputs
p = inputParser;
addParameter(p,'r'              ,0.5              ,@isnumeric);
addParameter(p,'transport_f'    ,'Parker'         ,@ischar);
addParameter(p,'resistance_f'   ,'Engelund&Hansen',@ischar);
addParameter(p,'c0'             ,[]               ,@isnumeric);
addParameter(p,'cD'             ,[]               ,@isnumeric);
addParameter(p,'cT'             ,[]               ,@isnumeric);
addParameter(p,'Fr0'            ,[]               ,@isnumeric);
addParameter(p,'Delta'          ,1.65             ,@isnumeric);
addParameter(p,'sec_flow'       ,false            ,@islogical);
parse(p,varargin{:});

%Assigning value to optional inputs
r              =p.Results.r;
transport_f    =p.Results.transport_f;
resistance_f   =p.Results.resistance_f;
c0             =p.Results.c0;
cD             =p.Results.cD;
cT             =p.Results.cT;
Fr0            =p.Results.Fr0;
Delta          =p.Results.Delta;
sec_flow       =p.Results.sec_flow;

   	
%% Definition of the linear system of equations

%Coefficients depending on the resistance and transport formulae
[c0,cD,cT,PhiD,PhiT]=derivatives(theta0,ds0,resistance_f,transport_f,c0,cD,cT);

%Definition of useful coefficients (see Camporeale et al., 2007)
f1=2/(1+2*cT);
f2=-2*cD/(1+2*cT);
P1=f1*PhiT;
P2=f2*PhiT+PhiD;

%Coefficients of the secondary flow (Struiksma, 1985)
if sec_flow==true
    von_kar=0.41; %Von Karman constant
    As=2/von_kar^2*(1-1/(von_kar*c0));
else
	As=0;
end

%Definition of symbolic variables
beta  =sym('beta');   %Channel aspect ratio
lambda=sym('lambda'); %Complex wavenumber

%If not user-provided, the reference Froude number is computed as a function of theta0 and ds0
if isempty(Fr0)
	Fr0=sqrt(ds0*Delta*theta0*c0^2); %Froude number
end

a6=r/(beta*sqrt(theta0));

%Matrix of the 4x4 linear system in the variables (U,V,D,H/Fr0^2)
M=[
[ lambda              , -pi/2.                       , lambda                 , 0                 ] %Continuity 
[ f1*beta/c0^2+lambda , 0                            , f2*beta/c0^2-beta/c0^2 , lambda            ] %X-Momentum
[ 0                   , beta/c0^2+lambda*(1+As/c0^2) , 0                      , pi/2              ] %Y-Momentum
[ P1*lambda           , -0.5*pi                      , lambda*P2-pi^2*a6/4    , Fr0^2*(pi^2*a6/4) ] %Exner
];

%Condition for obtaining a non-trivial solution
eq=det(M); %Determinant of the matrix (quartic polynomia in lambda)


%% Calculation of resonant point

[beta_expr,params,conds]=solve(eq,beta,'ReturnConditions',true); %relation beta(lambda) (complex expression) 

%Seeking imaginary (i.e. no spatial damping/growing) wavenumber for which beta is real (i.e. vanishing imaginary part)
wavenumber_init=pi/2*sqrt(r)/(c0*theta0^(1/4))*sqrt(f1/(1-P2)); %Initial wavenumber from the approximated ZS2 model
options = optimset('Display','off');
wavenumber_R=fsolve(@(wavenumber) double(imag((subs(beta_expr(1),lambda,wavenumber*i)))),wavenumber_init,options);

%Resonance value of beta is the one corresponding to wavenumber_R
beta_R=double(abs(real(subs(beta_expr(1),lambda,wavenumber_R*i))));


%% Calculation of the four eigenvalues for each Fourier mode 

disp('Computing eigenvalues..')

%Matrix of the system in the variables U,V,D,eta    
M2(:,1:2)=M(:,1:2); %First two columns are the same as in M
M2(:,3)=M(:,3)+M(:,4)/Fr0^2;
M2(:,4)=M(:,4)/Fr0^2;

%Matrix containing the coefficients u,v,d (as a symb function of beta and lambda)
C=-inv(M2(1:3,1:3))*M2(1:3,4);

for m=1:N

    disp(sprintf('Fourier mode m=%d',m))

	eq_lambda=subs(eq,beta,beta0/m);
	eq_lambda=vpa(eq_lambda); %Evaluate coefficient
	
	%Calculation of the four eigenvalues
	lambda_sol=solve(eq_lambda);
	[aux,ind]=sort(real(lambda_sol)); %Sorting based on the real part
    lambda_sol=lambda_sol(ind);

	lambda_mj(m,1)=eval(lambda_sol(1)); %Eigenvalue 1 (double precision)
	lambda_mj(m,2)=eval(lambda_sol(2));
	lambda_mj(m,3)=eval(lambda_sol(3));
	lambda_mj(m,4)=eval(lambda_sol(4));

	%Substituting beta leads to a function la lambda only
    C_lambda=subs(C,beta,beta0/m);
    
	%Coefficients (1,u,v,d)
    for j=1:4	
        aux=subs(C_lambda,lambda,lambda_mj(m,j));
        u_mj(m,j)=+eval(aux(1));
        v_mj(m,j)=-eval(aux(2));
        d_mj(m,j)=+eval(aux(3));
    end
	lambda_mj(m,:)=lambda_mj(m,:)*m; %Eigenvalue 1 (double precision)
	
end    


return
