% PLOTTING DIMENSIONLESS VELOCITY FIELD AND STREAMLINES
% Modified on: 14-Aug-2019

%Definition of global maps
X=[XA XB(:,2:end)];
Y=[YA YB(:,2:end)];
U=[U_A U_B(:,2:end)];
V=[V_A V_B(:,2:end)];
D=[D_A D_B(:,2:end)];
eta=[eta_A eta_B(:,2:end)];


%% 1) Plotting streamlines

%Definition of initial points
Nstream=30; %Number of streamlines

%Initial x-coordinate: input section
start_x=-L_A*ones(1,Nstream); 

%Initial y-coordinates: at intervals having the same water flux

%Cumulative discharge across the entrance section
dy=2/(Ny-1); %Grid spacing
q_int=cumtrapz(U_A(:,1).*D_A(:,1))*dy;

start_q_int=linspace(0,q_int(end),Nstream); %Regularly spaced q_int points
start_y=interp1(q_int,y,start_q_int); %y-coordinates corresponding to q_int points

figure
	colormap autumn
	pcolor(X,Y,eta)
	shading interp	
	colorbar
	hold on
	streamline(X,Y,U,V,start_x,start_y)
    plot([0 0],[2-delta,2],'-k','LineWidth',2)
    hold off    

    title('Depth-averaged streamlines')
	grid on
	xlim([-L_A L_B])
	ylim([0 2])
    xlabel('x [-]')
    ylabel('y [-]')

	printpdf(['Figures/Streamlines.pdf'],[-1.5 0.2 2.8 -0.4],gcf,[30 6],'-r600','zbuffer')


%% 2) Plotting velocity vector field

res=[20,40]; %Resolution of arrows

figure
	colormap autumn
	pcolor(X,Y,eta)
	shading interp	
	colorbar
	hold on
	quiver(imresize(X,res,'bilinear'),imresize(Y,res,'bilinear'),imresize(U,res,'bilinear'),imresize(V,res,'bilinear'),'b')
    plot([0 0],[2-delta,2],'-k','LineWidth',2)
    hold off    

    title('Depth-averaged velocity field')
	xlim([-L_A L_B])
	ylim([0 2])
    shading interp
    xlabel('x [-]')
    ylabel('y [-]')
	
	printpdf(['Figures/Velocity_field.pdf'],[-2 0.2 2.5 -0.4],gcf,[30 6],'-r600','zbuffer')
