%Printing of pdf images for latex use
%Author: Marco Redolfi
%Created on: 24-Oct-2013

%Version 2.0 12-Nov-2013
%	The paper position is now proportional to the paper size
%Version 3.0 03-Dec-2014
%	Stretched transverse coordinate


function printpdf(fname,position_rel,fig,pagesize,resolution,renderer)
	
	%REQUIRED PARAMETERS
	%fname: file (or complete path) name; extension not needed
	
	%OPTIONAL PARAMETERS
	%position_rel: position relative to the default one
	%	usually only small adjustments are needed to fit the figure into the page
	%fig: figure name
	%pagesize: size of the paper in [cm]
	%resolution: like '-r1200'
	%Rendered: 'painters' (default), 'zbuffer' or 'openGL'
	
	if nargin==6
		set(fig, 'renderer', renderer);
	else
		if nargin<4
			pagesize=[15 10];
			disp('Default page size 10x15 cm')
		end
		if nargin<3
			fig=gcf;
		end	
		if nargin<2
			position_rel=[0 0 0 0];
			disp('Default paper position [-0.6 -0.2 16.8 10.5] cm')
		end	
	end	

	set(fig, 'PaperUnits', 'centimeters');
	set(fig, 'PaperSize', pagesize); 
	%The \textwidth width in latex is 12.13cm; 
	%nevertheless printing on a larger (10x15cm) page produces the correct fontsize 
	set(fig, 'PaperPositionMode', 'manual');
	
	%The position of the figure on the page is scaled with the dimension of
	%the paper. The coefficients are chosen in order to position a figure
	%in a 15x10 paper without too much margins
	
	%Since the margin depends on the features of each single figure a manual
	%adjustment (through position_rel) is often necessary
	
	position=[pagesize(1)*(-0.6/15) pagesize(2)*(-0.2/10) pagesize(1)*(1+1.8/15) pagesize(2)*(1+0.5/10)]+position_rel;
	
	set(fig, 'PaperPosition',position); %These values have to be manually adjusted

	if nargin<5
		print(fig,'-dpdf',fname)
	else
		print(gcf,'-dpdf',resolution,fname)
	end	

end
