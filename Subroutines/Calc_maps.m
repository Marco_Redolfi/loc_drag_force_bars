%% CALCULATING SOLUTION ON A 2-D GRID
%Modified on: 19-Aug-2019

disp('Computing maps..')

%Creating regular grids for the two channels
x_A=linspace(-L_A,0,Nx_A);
x_B=linspace(0,L_B,Nx_B);
y=linspace(0,2,Ny);

[XA,YA]=meshgrid(x_A,y); %Matrix of x-coordinates
[XB,YB]=meshgrid(x_B,y); %Matrix of y-coordinates

%Initialization of variables to basic flow
S0=theta0*Delta*ds0;
eta_A=-beta0*S0*XA;
eta_B=-beta0*S0*XB;
if remove_slope
    eta_A=0*eta_A;
    eta_B=0*eta_B;
end

U_A=ones(Ny,Nx_A);
U_B=ones(Ny,Nx_B);
V_A=zeros(Ny,Nx_A);
V_B=zeros(Ny,Nx_B);
D_A=ones(Ny,Nx_A);
D_B=ones(Ny,Nx_B);

%% 1-D solution
A0=delta/2;
eta_A=eta_A +f*A0*Fr0^2;

%% 2-D solution
for m=1:N
	
	%% Channel A
	
	%Matrixes of the transverse and longitudinal structure 
	Cos=cos(m*pi*YA/2);
	Sin=sin(m*pi*YA/2);
	
    ind=real(lambda(m,:))>0;
    jA=find(ind);
    jB=find(~ind);

    for j=jA
        E=exp(lambda(m,j)*XA);
	    eta_A=eta_A +f*real(        S(m,j)*Cos.*E );
        U_A  =U_A   +f*real( u(m,j)*S(m,j)*Cos.*E );
	    V_A  =V_A   +f*real( v(m,j)*S(m,j)*Sin.*E );
	    D_A  =D_A   +f*real( d(m,j)*S(m,j)*Cos.*E );
    end

    	
	%% Channel B
	
	%Matrixes of the transverse and longitudinal structure
	Cos=cos(m*pi*YB/2);
	Sin=sin(m*pi*YB/2);

    for j=jB
        E=exp(lambda(m,j)*XB);
	
		eta_B=eta_B +f*real(       S(m,j)*Cos.*E);
		U_B  =U_B   +f*real(u(m,j)*S(m,j)*Cos.*E);
		V_B  =V_B   +f*real(v(m,j)*S(m,j)*Sin.*E);
		D_B  =D_B   +f*real(d(m,j)*S(m,j)*Cos.*E);
	
	end
	
end
