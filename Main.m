%****************************************************************************************
% LINEAR SOLUTION FOR STEADY ALTERNATE BARS FORCED BY A LOCALIZED ASYMMETRIC DRAG FORCE
% Author: Marco Redolfi (marco.redolfi@unitn.it)
% Open Source code, distributed under GNU General Public Licence (GPLv3)
% Please refer to: Redolfi et al. (2021), Journal of Fluid Mechanics
%****************************************************************************************

close all 
clear all 
clc
addpath('Subroutines')


%% DATA INPUT ***********************************

%Reference flow characteristics
beta0=7.8/2;  %HALF width-to-depth ratio
ds0=0.00974;  %Dimensionless sediment diameter
theta0=0.125; %Shields number
c0=9.6;       %Chézy coefficient

%Forcing parameters
f=0.168; %Dimensionless drag force per unit width
delta=1; %Dimensionless size of the grid

%Model parameters
N=10;                           %Number of Fourier modes
transport_f='MP&M';             %Transport formula
resistance_f='Engelund&Hansen'; %Flow resistance formula
sec_flow=false;                 %If true the spiral flow is considered as in Struiksma (1985)
Delta=1.65;                     %Relative submerged weight of sediments
r=0.5;	                        %Ikeda parameter for effect of transverse slope on qs direction

%Domain dimension and plotting resolution
L_A=6;             %Dimensionless length of the channel A 
L_B=15;            %Dimensionless length of the channel B
Nx_A=120;          %Number of points in the x-direction (channel A)
Nx_B=300;          %Number of points in the x-direction (channel B)
Ny=50;	           %Number of points in the y-direction
remove_slope=true; %If true the mean slope is removed from the bottom profile


%% CALCULATIONS *********************************

%% Computation of basic flow parameters

%Froude number of the reference flow
Fr0=c0*sqrt(Delta*theta0*ds0);

%Separating contributions of the skin and drag friction
c0_skin = fsolve(@(x) 6 + 2.5 * log((1/(2.5*ds0))*(c0/x)^2) - x,c0); %Chézy parameter associated to the skin drag
theta0_skin=Fr0^2/(c0_skin^2*Delta*ds0); %Shields stress due to skin drag

%% Computation of eigenvalues
[lambda,u,v,d,beta_R,~]=eigenvalues(beta0,theta0_skin,NaN,N,'r',r,'Delta',Delta,'c0',c0,'Fr0',Fr0,...
    'transport_f',transport_f,'resistance_f',resistance_f,'sec_flow',sec_flow);

%% Solution of the linear system for the different Fourier modes
S=solution(N,u,v,d,lambda,delta,Fr0);

%% Point-by-point calculation of the maps for the primary variables 
Calc_maps


%% PLOTTING *************************************

%% Dimensionless solution

Plotting %Color maps and longitudinal profiles
Plotting_velocity_field %Velocity field and streamlines


%% Dimensional solution

%Problem dimensions
Bstar=0.45;  %HALF channel width [m]
Ustar=0.537; %Reference velocity [m/s]
gamma=9810;  %Specific weight of water [N/m^3]

Plotting_dimensional %Color maps and longitudinal profiles
